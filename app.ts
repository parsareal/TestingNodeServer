import { Server } from "bacjs";
import Config from "./bac.config";

let server = new Server(Config);

server.start();
