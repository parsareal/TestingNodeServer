import { IConfig } from "bacjs";

let config: IConfig = {
    routers: [
        { prefix: "/api", route: "./controllers/todo" }
    ],

    port: 9000,

    mongo: {
        connection: "mongodb://localhost/test"
    },

    typeScript: {
        outDir: "/out"
    }

};

export default config;
