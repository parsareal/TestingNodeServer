import { Controller, RequestType } from "bacjs";
import { Request, ResponseToolkit } from "hapi";
import Todo from "../models/Todo";

export default class TodoController extends Controller {
    public init(): void {
        this.assign("", [RequestType.GET], this.getAllTodos);
        this.assign("/add", [RequestType.POST], this.addNewTodo);
    }

    private async getAllTodos(req: Request, h: ResponseToolkit) {
        let query = Todo.find({});
        let result = await query.exec();

        return result;
    }

    private async addNewTodo(req: Request, h: ResponseToolkit) {
        let payload = req.payload as any;

        let newTodo = new Todo({
            title: payload.title,
            id: payload.id,
            description: payload.description
        });

        let result = await newTodo.save();
        return result;
    }
}
