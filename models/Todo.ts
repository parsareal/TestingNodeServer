import { DBModel } from "bacjs";
import { SchemaDefinition, Document } from "mongoose";

class Todo extends DBModel {
    public static $schema(): SchemaDefinition {
        return {
            title: String,
            id: {type: String, index: true, required: true, unique: true},
            description: String
        }
    }
}

export interface ITodoSchema extends Document {
    title: string;
    id: string;
    description: string;
}

export default Todo.$model<ITodoSchema>();
